---
title: 这是标题：演示用的新发表文章

date: 2017-12-29
categories:
- 类别1
- 类别2
tags:
- 标签1
- 标签2
-keywords:
- 关键词1
- 提供给搜索引擎
thumbnailImagePosition: left
thumbnailImage: //d1u9biwaxjngwg.cloudfront.net/chinese-test-post/vintage-140.jpg
---

这一行文字只会显示在主页面上，剩下的内容会隐藏.................取决于下方的标签
<!--more-->

# 标题

## 欢迎使用我上传的方便包，下面是目录....取决于下方的标签
-------------------
<!-- toc -->
-------------------



### 使用hugo快速方便地建立属于你自己的网站和blog！

这是一个用来folks快速克隆搭建个人网站的小项目，其中选取了几个比较流行或者美观的主题以供你选择。你只需要简单的folks该项目，然后根据教程稍作修改，不需要烦人的命令行，不需要复杂的步骤和难懂操作即可完成网站的搭建。

> 特别说明，仅适用于跟我一样的懒人玩家，该项目不保证更新，^_^

-------------------



#### 包含以下主题：

- beautifulhugo
- bilberry-hugo-theme
- docuapi
- hugo-academic
- hugo-future-imperfect
- hugo-material-docs
- hugo-nuo
- hugo-pacman-theme
- hugo-theme-docdock
- hugo-theme-dream
- hugo-theme-learn
- hugo-tranquilpeak-theme
- hurock
- hyde
- hyde-x
- prologue

下面是源文件
-------------------




```markdown

---
title: 这是标题：演示用的新发表文章

date: 2017-12-29
categories:
- 类别1
- 类别2
tags:
- 标签1
- 标签2
-keywords:
- 关键词1
- 提供给搜索引擎
thumbnailImagePosition: left
thumbnailImage: //d1u9biwaxjngwg.cloudfront.net/chinese-test-post/vintage-140.jpg
---

这一行文字只会显示在主页面上，剩下的内容会隐藏.................取决于下方的标签
<!--more-->

# 标题

## 欢迎使用我上传的方便包，下面是目录....取决于下方的标签
-------------------
<!-- toc -->
-------------------



### 使用hugo快速方便地建立属于你自己的网站和blog！

这是一个用来folks快速克隆搭建个人网站的小项目，其中选取了几个比较流行或者美观的主题以供你选择。你只需要简单的folks该项目，然后根据教程稍作修改，不需要烦人的命令行，不需要复杂的步骤和难懂操作即可完成网站的搭建。

> 特别说明，仅适用于跟我一样的懒人玩家，该项目不保证更新，^_^

-------------------



#### 包含以下主题：

- beautifulhugo
- bilberry-hugo-theme
- docuapi
- hugo-academic
- hugo-future-imperfect
- hugo-material-docs
- hugo-nuo
- hugo-pacman-theme
- hugo-theme-docdock
- hugo-theme-dream
- hugo-theme-learn
- hugo-tranquilpeak-theme
- hurock
- hyde
- hyde-x
- prologue

```
